import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import router from './plugins/vue-router';
import axios from 'axios';

axios.defaults.baseURL = 'http://127.0.0.1/api/v1/';
// axios.defaults.baseURL = '/api/v1/';

Vue.config.productionTip = false;

new Vue({
    vuetify,
    router,
    render: h => h(App)
}).$mount('#app');
